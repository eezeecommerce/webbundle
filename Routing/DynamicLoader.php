<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\WebBundle\Routing;

use eezeecommerce\SettingsBundle\Entity\Settings;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Doctrine\ORM\EntityManager;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;

class DynamicLoader extends Loader
{

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Settings
     */
    private $setting;

    /**
     * DynamicLoader constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param SettingsProvider $settings
     */
    public function setSettings(SettingsProvider $settings)
    {
        $this->settings = $settings->loadSettingsBySite();
    }

    /**
     * @param mixed $resource
     * @param null $type
     *
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException("Do not add the dynamic loader twice");
        }

        $categories = $this->em->getRepository("eezeecommerceCategoryBundle:Category")
                ->findAll();

        $routes = new RouteCollection();

        $defaults = array(
            '_controller' => "eezeecommerceCategoryBundle:Category:index",
        );

        foreach ($categories as $category) {
            if ($category->getLvl() !== 0) {
                continue;
            }

            if (count($category->getChildren()) > 0) {
                $check = false;
                foreach ($category->getChildren() as $children) {
                    if (!$check) {
                        if (((count($children->getProduct()) < 1 && count($children->getChildren()) < 1)) && $this->settings->getCategoryShowEmpty() === false && !$children->getShowWhenEmpty()) {
                            continue;
                        } else {
                            $check = true;
                        }
                    }

                    if (true === $children->getDisabled()) {
                        continue;
                    }

                    if (count($children->getChildren()) > 0) {
                        foreach ($children->getChildren() as $subChildren) {
                            if (null !== $subChildren->getSlug()) {
                                $path = $category->getSlug()->getSlug() . "/" . $children->getSlug()->getSlug() . "/" . $subChildren->getSlug()->getSlug();
                                $route = new Route($path, $defaults);
                                $routeName = $subChildren->getSlug()->getUriKey();
                                $routes->add($routeName, $route);
                            }
                        }
                    }
                    if (null !== $children->getSlug()) {
                        $path = $category->getSlug()->getSlug() . "/" . $children->getSlug()->getSlug();
                        $route = new Route($path, $defaults);
                        $routeName = $children->getSlug()->getUriKey();
                        $routes->add($routeName, $route);
                    }
                }
            }
            $path = $category->getSlug()->getSlug();
            $route = new Route($path, $defaults);
            $routeName = $category->getSlug()->getUriKey();
            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return "doctrine" === $type;
    }

}
