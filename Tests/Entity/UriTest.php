<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16/06/16
 * Time: 11:39
 */

namespace eezeecommerce\WebBundle\Tests\Entity;


use eezeecommerce\WebBundle\Entity\Uri;

class UriTest extends \PHPUnit_Framework_TestCase
{
    public function testSlugWithQuotesIsRemovedAndLowerCase()
    {
        $uri = new Uri();

        $actual = "This's is a test";

        $uri->setSlug($actual);

        $expected = "thiss-is-a-test";

        $this->assertEquals($expected, $uri->getUriKey());
    }

    public function testSettingSlugTwiceDoesNotOverrideKey()
    {
        $uri = new Uri();

        $actual = "This's is a test";

        $uri->setSlug($actual);

        $expected = "thiss-is-a-test";

        $uri->setSlug("This's a second Test");

        $this->assertEquals($expected, $uri->getUriKey());
    }
}