<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\WebBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="uri")
 * @ORM\Entity(repositoryClass="eezeecommerce\WebBundle\Entity\UriRepository")
 */
class Uri
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column(name="title", type="string", length=60)
     */
    private $title;
    
    /**
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;
    
    /**
     * @ORM\Column(name="keywords", type="string", length=255)
     */
    private $keywords;
    
    /**
     * @ORM\Column(name="description", type="string", length=160)
     */
    private $description;
    
    /**
     * @ORM\Column(name="controller", nullable=true, type="string", length=255)
     * @
     */
    private $controller;
    
    /**
     * @ORM\Column(name="action", nullable=true, type="string", length=255)
     */
    private $action;

    /**
     * @ORM\Column(type="string")
     */
    private $uri_key;
    
    /**
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"url"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;
    
    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\ProductBundle\Entity\Product", inversedBy="uri", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;
    
    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\CategoryBundle\Entity\Category", inversedBy="slug", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Uri
     */
    public function setTitle($title)
    {
        $this->title = $title;
        

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Uri
     */
    public function setUrl($url)
    {
        $this->url = $url;
        
        $this->setSlug($url);

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Uri
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Uri
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Uri
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        if (null === $this->uri_key) {
            $this->setUriKey($slug);
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set controller
     *
     * @param string $controller
     *
     * @return Uri
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Uri
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return Uri
     */
    public function setProduct(\eezeecommerce\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \eezeecommerce\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set category
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $category
     *
     * @return Uri
     */
    public function setCategory(\eezeecommerce\CategoryBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \eezeecommerce\CategoryBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return Uri
     */
    public function setUriKey($uri_key)
    {
        $uri_key =
            preg_replace("/[^a-z0-9.-]+/i", "",
                preg_replace("/\s/", "-",
                    strtolower($uri_key)
                )
            )
            ;
        $this->uri_key = $uri_key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getUriKey()
    {
        return $this->uri_key;
    }
}
